const btn = document.querySelector(".page-header__nav__buttons");
btn.addEventListener("click", (events) => {
	const ul = document.querySelector(".page-header__menu")
	ul.classList.toggle("page-header__menu--active");
	const svg = btn.querySelectorAll("svg");
	svg.forEach((elem) => {
		elem.classList.toggle("nav-btn-open");
	})
})

